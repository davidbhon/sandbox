#!/bin/bash
# .profile or .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ] ; then
  . ~/.bashrc
fi

if [ -f ~/.balias ] ; then
  . ~/.balias
fi

# User specific environment and startup programs
# export PATH=$HOME/bin:/opt/bin:/opt/conda2/bin:$PATH
export PAGER=more
export KUBECONFIG=${HOME}/.kube/admin.conf
export GOROOT=/opt/go
export GOPATH=/opt/aws/bin:${GOROOT}/bin
#export PATH=${HOME}/bin:/opt/conda2/bin:/opt/bin:${GOPATH}:${PATH}
export PATH=${HOME}/bin:/opt/conda2/bin:${GOPATH}:${PATH}


if [[ $SHLVL > 1 ]] ; then 
  echo "SHLVL == $SHLVL" 
  return
fi

monyr=`date "+%b%Y"`
monyr=`echo $monyr | tr [:upper:] [:lower:]` # monyr=`echo $monyr | sed 's/[[:upper:]]*/\L&/g'`

monyr=jun2018

echo pushd to ~/${monyr}

\mkdir -p ~/${monyr} >& /dev/null
pushd ~/${monyr} 

echo exec ipython --no-banner --profile=bash ... should only take a coupla sec. ....
echo -n 'or use alias ipy ... ' ; alias ipy
#exec ipython --no-banner --profile=bash
#ipython --no-banner --profile=bash
#
