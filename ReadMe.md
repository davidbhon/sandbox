The gitlab new project create repository is initally empty.

Below is cut-n-pasted from the gitlab new project (post) creation success page ...

Note when creating a new (empty) project repo. the gitlab menu offers a list of user and group names to choose as an org.
If one wishes to change the project org. of a specific repo., navigate to the gitlab advanced settings page and select "Transfer Project"
and "Select a new namespace". The transfer should issue an email with guidance for down stream clones:

Project davidbhon/openstack was moved to another location
The project is now located under DevCloudOps / openstack

To update the remote url in your local repository run (for ssh):
git remote set-url origin git@gitlab.com:DevCloudOps/openstack.git

or for http(s):
git remote set-url origin https://gitlab.com/DevCloudOps/openstack.git

...

Also, by default git push will prompt for user and password:
Username for 'https://gitlab.com': davidbhon
Password for 'https://davidbhon@gitlab.com': jaked0gg

To avoid the prompt (default timeout is 900 sec -- 15 min ... only units of sec. supported? what's the max?):

git config credential.helper store
git config --global credential.helper 'cache --timeout 604800'

Above sets credential cache timeout to 1 week (hopefully).

---

If you already have files you can push them using command line instructions below.

Otherwise you can start with adding a README, a LICENSE, or a .gitignore to this project.

You will need to be owner or have the master permission level for the initial push, as the master branch is automatically protected.

You can activate Auto DevOps (Beta) for this project.

It will automatically build, test, and deploy your application based on a predefined CI/CD configuration.

Command line instructions

Git global setup
git config --global user.name "david b. hon"
git config --global user.email "davidb.hon@gmail.com"

Create a new repository
git clone --recursive https://gitlab.com/davidbhon/sandbox.git
cd sandbox
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/davidbhon/sandbox.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/davidbhon/sandbox.git
git push -u origin --all
git push -u origin --tags

---
