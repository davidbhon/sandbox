

How to catch and process Linux core dumps in Your own application

Since kernel 2.6.19 Linux supports piping core dumps to a certain application instead of just writing them to a file. What happens when a core is to be dropped depends on the setting in /proc/sys/kernel/core_pattern. If a path is preceded with a | (pipe) and then a path to an application, then in case of a crash it gets run and output (stdout) from the kernel is redirected to it. Otherwise the core is dumped to a file directly and the entry in core_pattern treated as a path (either absolute – starts with ‘/’ or relative).

Applications such as apport (Ubuntu) or abrt-ccpp (RedHat) choose the first option of getting the data through a pipe from the kernel.
Let’s see some examples:
user@host:~$ cat /proc/sys/kernel/core_pattern

|/usr/share/apport/apport %p %s %c %P
This entry tells the kernel that the /usr/share/apport/apport application wants to get the output through STDIN from the kernel, with some parameters (argv): process PID, signal which caused coredump, core file size soft resource limit, process PID (initial PID namespace). The meaning of all of the options is described here: http://man7.org/linux/man-pages/man5/core.5.html

A different example would be:
user@host:~$ cat /proc/sys/kernel/core_pattern

/tmp/core.%p.%e

This time the coredump would be written directly to a file in /tmp/ called core.&lt;pid&gt;.&lt;executable_filename&gt;.

Writing Your own application for intercepting coredumps is useful for:
– reading data about a process such as memory maps: /proc/pid/maps before the crash occurs and the kernel removes the /proc entries.
– Having Your own notification of crashes occuring in a certain application (that’s what apport in Ubuntu is for)

A simple piece of code for just getting the core dump data from the kernel, writing it to a certain location and finally logging it to a file may look like that:

C


/* A Linux core-dumping application example
 * Author: Wacław Dziewulski (wacekdziewulski@gmail.com)
 * 
 * To use, set the /proc/sys/kernel/core_pattern file to the following string:
 * |<path_to_this_binary> %p %e %s
 */

#include 
#include <linux/limits.h> /* PATH_MAX definition */
#include        /* strncpy() definition */

#define BUFFER_SIZE 8192
#define DUMP_FILE_FORMAT "cdump.%s.%d"
#define LOG_PATH "/tmp/coredumper.log"

static FILE* log_file;

typedef struct DumpMetadata_t {
        int pid;
        int signal;
        char executable[PATH_MAX];
} DumpMetadata;

/* Forward declarations */
void openLogFile();
void closeLogFile();

#define LOG(...) \
                if (log_file == NULL) { \
                        openLogFile(LOG_PATH); \
                } \
                if (log_file != NULL) { \
                        fprintf(log_file, __VA_ARGS__); \
                }

void openLogFile(const char* path) {
        if (log_file == NULL) {
                log_file = fopen(path, "a");
        }
}

void closeLogFile() {
        if (log_file != NULL) {
                fclose(log_file);
        }
}

int saveDump(const char* filename) {
        char input_buffer[BUFFER_SIZE];

        LOG("Writing core file...\n");
        /* Open the file to which we will write the dump file. Remember to open in write/binary mode ! */
        FILE* dump_file = fopen(filename, "wb");
        if (dump_file != NULL) {
                int read;
                /* Read the dump data from STDIN and write it to our dump file. */
                while ((read = fread(input_buffer, 1, BUFFER_SIZE, stdin)) > 0) {
                        /* Basic check for write errors */
                        if (fwrite(input_buffer, 1, read, dump_file) <= 0) {
                                LOG("Failed to write dump data. Giving up !\n");
                                return -1;
                        }
                }
                fclose(dump_file);
                return 0;
        }

        LOG("Failed to open dump file '%s' for writing. Giving up !\n", filename);
        return -1;
}

int main(int argc, char** argv) {
        DumpMetadata dump_data;
        char dump_filename[PATH_MAX];
        openLogFile(LOG_PATH);

        LOG("CoreDumper application started...\n");

        if ((argc-1) != 3) {
                LOG("Incorrect parameters for coredumper, expected '3', got '%d'. Giving up !\n", argc-1);
                return -1;
        }

        dump_data.pid = atoi(argv[1]);
        strncpy(dump_data.executable, argv[2], PATH_MAX);
        dump_data.signal = atoi(argv[3]);

        LOG("Caught '%s', pid '%d' process crash with signal: '%d'\n", dump_data.executable, dump_data.pid, dump_data.signal);

        snprintf(dump_filename, PATH_MAX, "/tmp/"DUMP_FILE_FORMAT, dump_data.executable, dump_data.pid);

        if (saveDump(dump_filename) == 0) {
                LOG("Dump file collected successfully.\n");
        }

        closeLogFile();
        LOG("CoreDumper application done.\n");

        return 0;
}
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
93
94
95
96
97
98
99
100
/* A Linux core-dumping application example
 * Author: Wacław Dziewulski (wacekdziewulski@gmail.com)
 * 
 * To use, set the /proc/sys/kernel/core_pattern file to the following string:
 * |<path_to_this_binary> %p %e %s
 */
 
#include 
#include <linux/limits.h> /* PATH_MAX definition */
#include        /* strncpy() definition */
 
#define BUFFER_SIZE 8192
#define DUMP_FILE_FORMAT "cdump.%s.%d"
#define LOG_PATH "/tmp/coredumper.log"
 
static FILE* log_file;
 
typedef struct DumpMetadata_t {
        int pid;
        int signal;
        char executable[PATH_MAX];
} DumpMetadata;
 
/* Forward declarations */
void openLogFile();
void closeLogFile();
 
#define LOG(...) \
                if (log_file == NULL) { \
                        openLogFile(LOG_PATH); \
                } \
                if (log_file != NULL) { \
                        fprintf(log_file, __VA_ARGS__); \
                }
 
void openLogFile(const char* path) {
        if (log_file == NULL) {
                log_file = fopen(path, "a");
        }
}
 
void closeLogFile() {
        if (log_file != NULL) {
                fclose(log_file);
        }
}
 
int saveDump(const char* filename) {
        char input_buffer[BUFFER_SIZE];
 
        LOG("Writing core file...\n");
        /* Open the file to which we will write the dump file. Remember to open in write/binary mode ! */
        FILE* dump_file = fopen(filename, "wb");
        if (dump_file != NULL) {
                int read;
                /* Read the dump data from STDIN and write it to our dump file. */
                while ((read = fread(input_buffer, 1, BUFFER_SIZE, stdin)) > 0) {
                        /* Basic check for write errors */
                        if (fwrite(input_buffer, 1, read, dump_file) <= 0) {
                                LOG("Failed to write dump data. Giving up !\n");
                                return -1;
                        }
                }
                fclose(dump_file);
                return 0;
        }
 
        LOG("Failed to open dump file '%s' for writing. Giving up !\n", filename);
        return -1;
}
 
int main(int argc, char** argv) {
        DumpMetadata dump_data;
        char dump_filename[PATH_MAX];
        openLogFile(LOG_PATH);
 
        LOG("CoreDumper application started...\n");
 
        if ((argc-1) != 3) {
                LOG("Incorrect parameters for coredumper, expected '3', got '%d'. Giving up !\n", argc-1);
                return -1;
        }
 
        dump_data.pid = atoi(argv[1]);
        strncpy(dump_data.executable, argv[2], PATH_MAX);
        dump_data.signal = atoi(argv[3]);
 
        LOG("Caught '%s', pid '%d' process crash with signal: '%d'\n", dump_data.executable, dump_data.pid, dump_data.signal);
 
        snprintf(dump_filename, PATH_MAX, "/tmp/"DUMP_FILE_FORMAT, dump_data.executable, dump_data.pid);
 
        if (saveDump(dump_filename) == 0) {
                LOG("Dump file collected successfully.\n");
        }
 
        closeLogFile();
        LOG("CoreDumper application done.\n");
 
        return 0;
}
The code is compiled without any special libraries:
gcc coredumper.c -o coredumper

Perl


#!/usr/bin/perl

# The input line to this app should be:
# /proc/sys/kernel/core_pattern = <coredumper.pl_path> %p %e %s

use strict;
use warnings;

our $CORE_DUMPER_LOG;
our $CORE_DUMPER_PATH = "/tmp/coredumper.log";

my $pid;
my $executable;
my $signal;

sub getCoreDump {
    my $coredump_path = "/tmp/my_core.$pid";

    open(CORE_DUMP, "<$coredump_path");
    while (<>) {
        print CORE_DUMP $_;
    }
    close(CORE_DUMP);

    return $coredump_path;
}

sub main {
    open($CORE_DUMPER_LOG, ">>$CORE_DUMPER_PATH") or die("Cannot open $CORE_DUMPER_PATH for writing");

    if (scalar(@ARGV) != 3) {
        die ("Ouch, expected 2 input parameters, got: '" . scalar(@ARGV) . "'\n");
    }

    $pid = $ARGV[0];
    $executable = $ARGV[1];
    $signal = $ARGV[2];

    print $CORE_DUMPER_LOG "Process '" . $executable . "' crashed with signal: '" . $signal . "'\n";

    my $coredump_path = getCoreDump();

    close $CORE_DUMPER_LOG;
}

main();
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
#!/usr/bin/perl
 
# The input line to this app should be:
# /proc/sys/kernel/core_pattern = <coredumper.pl_path> %p %e %s
 
use strict;
use warnings;
 
our $CORE_DUMPER_LOG;
our $CORE_DUMPER_PATH = "/tmp/coredumper.log";
 
my $pid;
my $executable;
my $signal;
 
sub getCoreDump {
    my $coredump_path = "/tmp/my_core.$pid";
 
    open(CORE_DUMP, "<$coredump_path");
    while (<>) {
        print CORE_DUMP $_;
    }
    close(CORE_DUMP);
 
    return $coredump_path;
}
 
sub main {
    open($CORE_DUMPER_LOG, ">>$CORE_DUMPER_PATH") or die("Cannot open $CORE_DUMPER_PATH for writing");
 
    if (scalar(@ARGV) != 3) {
        die ("Ouch, expected 2 input parameters, got: '" . scalar(@ARGV) . "'\n");
    }
 
    $pid = $ARGV[0];
    $executable = $ARGV[1];
    $signal = $ARGV[2];
 
    print $CORE_DUMPER_LOG "Process '" . $executable . "' crashed with signal: '" . $signal . "'\n";
 
    my $coredump_path = getCoreDump();
 
    close $CORE_DUMPER_LOG;
}
 
main();
Testing our app

The first thing we have to do is to convince Linux to use our application instead of the default one:
user@host:~/source/coredumper$ gcc coredumper.c -o coredumper

user@host:~/source/coredumper$ sudo su -

[sudo] password for user:

root@host:~# echo "|/home/user/source/coredumper/coredumper %p %e %s" > /proc/sys/kernel/core_pattern

root@toshivax:~# cat /proc/sys/kernel/core_pattern

|/home/user/source/coredumper/coredumper %p %e %s

This way we’ve registered our application in the kernel. Whenever there is a crash, our program will be called to collect the dump file. Let’s test it !
Let’s go into the terminal and try the following:
vax@toshivax:~/source/coredumper$ kill -SIGSEGV $$

Ok, so we’ve killed our own bash console with signal 11 (Segmentation Fault). If we’re lucky, our application worked and collected the dump file. Let’s see:
user@host:~$ cd /tmp/

user@host:/tmp$ ls

cdump.bash.16813  coredumper.log

user@host:/tmp$ tail coredumper.log

CoreDumper application started...

Caught 'bash', pid '21107' process crash with signal: '11'

Writing core file...

Dump file collected successfully.

user@host:/tmp$ gdb --core cdump.bash.21107

GNU gdb (Ubuntu 7.7-0ubuntu3.1) 7.7

Copyright (C) 2014 Free Software Foundation, Inc.

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

This is free software: you are free to change and redistribute it.

There is NO WARRANTY, to the extent permitted by law.  Type "show copying"

and "show warranty" for details.

This GDB was configured as "x86_64-linux-gnu".

Type "show configuration" for configuration details.

For bug reporting instructions, please see:

<http://www.gnu.org/software/gdb/bugs/>.

Find the GDB manual and other documentation resources online at:

<http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".

Type "apropos word" to search for commands related to "word".

[New LWP 21107]

Core was generated by `bash'.

Program terminated with signal SIGSEGV, Segmentation fault.

#0  0x00007fef0f4f0277 in ?? ()

(gdb) bt

#0  0x00007fef0f4f0277 in ?? ()

#1  0x000000000043789b in ?? ()

#2  0x0000000000000000 in ?? ()

(gdb)

Ok, so the application dumped the core file correctly and we can read it.

Note:
Sometimes core dumps are not written at all by the kernel. The most probable reason is the core file size limitation set by ulimit.

Try doing:
ulimit -c unlimited
to see if the core dump will now be generated. This by the way sets the core file size limit to ‘unlimited’. Unless You want to keep it this way, it’s wise to have any limit, since a coredump may eat a lot of Your disk space.

Where to go from here ?

We’ve got a basic application, which doesn’t do much better than the regular kernel mechanism for collecting core dumps. Nevertheless we can easily extend it to read and analyze the stacktrace automatically using various gdb options or using a C library such as libunwind. We can also add various forms of reporting the coredumps after extracting necessary information like apport in Ubuntu does.
