
http://www.alexonlinux.com/how-to-handle-sigsegv-but-also-generate-core-dump

How to handle SIGSEGV, but also generate a core dump
Posted on February 8, 2009, 12:03 am, by Alexander Sandler, under Blog, Howto, Short articles.
Recently I ran into this problem. How do you capture SIGSEGV with a signal handler and still generate a core file?

The problem is that once you have your own signal handler for SIGSEGV, Linux will not call default signal handler which generates the core file. So, once you got SIGSEGV, consider all that useful information about about origin of the exception, lost.

Luckily, there’s a solution. Here’s what I did.

You start with registering a signal handler. Once you get the signal, inside of the signal handler, set signal handler for the signal to SIG_DFL. Then send yourself same signal, using kill() system call. Here’s a short code snippet that demonstrates this little trick in action.

01
#include <stdio.h>
02
#include <sys/types.h>
03
#include <unistd.h>
04
#include <signal.h>
05
 
06
void sighandler(int signum)
07
{
08
    printf("Process %d got signal %d\n", getpid(), signum);
09
    signal(signum, SIG_DFL);
10
    kill(getpid(), signum);
11
}
12
 
13
int main()
14
{
15
    signal(SIGSEGV, sighandler);
16
    printf("Process %d waits for someone to send it SIGSEGV\n",
17
        getpid());
18
    sleep(1000);
19
 
20
    return 0;
21
}
Note that this code doesn’t actually cause a segmentation fault. To simulate segmentation fault, I did kill -11 <pid> from the command line. This is what happened.

$ ls
sigs.c
$ gcc sigs.c
$ ./a.out
Process 2149 waits for someone to send it SIGSEGV
Process 2149 got signal 11
Segmentation fault (core dumped)
$ ls
a.out*  core  sigs.c
Obviously, without lines 9 and 10 in the code, there would not be core file.

By the way, you can use this technique to handle any core generating exception – SIGILL, SIGFPE, etc.

http://www.alexonlinux.com/how-to-handle-sigsegv-but-also-generate-core-dump

I see you are re-signallying the same signal. Since the original signal is not guaranteed to be one providing a core dump have you considered:
gcore(), abort(), or { char *cp = 0; *cp = ‘1’; }
all of which seem to be fairly short and highly portable. Watch out for the last one if you have a SIGBUS or SIGSEGV handlers installed. Also you may need to uset system() or fork/exec to run the gcore utility on linux since gcore does not seem to be standard on linux yet.
