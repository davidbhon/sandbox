#!/bin/bash
set -o noclobber

export EDITOR=atom
export PAGER=more

export ANSIBLE_CONFIG=~/.ansible/ansible.cfg
export ANSIBLE_HOSTS=~/.ansible/hosts

export JVM=/opt/jvm
export PATH=${JVM}/scala/bin:${JVM}/groovy/bin:${JVM}/ant/bin:${JVM}/mvn/bin:/opt/bin:${PATH}
export LD_LIBRARY_PATH=${JVM}/scala/lib:${JVM}/groovy/lib:${JVM}/ant/lib:${JVM}/mvn/lib:${LD_LIBRARY_PATH}

export GOROOT=/usr/local/go && export GOPATH=$HOME/gowork
export PATH=${GOROOT}:${PATH}
export LD_LIBRARY_PATH=${GOROOT}/lib:${LD_LIBRARY_PATH}

export PY3=/opt/conda3
export PATH=${HOME}/bin:${PATH}:${PY3}/bin:/opt/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
export LD_LIBRARY_PATH=${HOME}/lib:${LD_LIBRARY_PATH}:/opt/lib:/usr/local/lib:/usr/lib

# an improperly set KRB5CCNAME really confuses kinit; in the event klist return nothing
# the resultant empty env. variable breaks kinit! so to be safe, make sure no env var exists!  
#export KRB5CCNAME=`klist -l|grep -iv expired|grep ICBR|awk '{print $2}'` ; echo $KRB5CCNAME ; klist -l
if [[ $KRB5CCNAME ]] ; then unset KRB5CCNAME ; fi

if [ -e ~/.balias ] ; then source ~/.balias ; fi

monyr=`date "+%b%Y"`
monyr=`echo $monyr | tr [:upper:] [:lower:]` # monyr=`echo $monyr | sed 's/[[:upper:]]*/\L&/g'`

echo pushd to ~/${monyr}

\mkdir -p ~/${monyr} >& /dev/null
pushd ~/${monyr} 

#echo exec ipython --no-banner --profile=bash ... should only take a coupla sec. ....
#exec ipython --no-banner --profile=bash
alias ipy
#ipython --no-banner --profile=bash


