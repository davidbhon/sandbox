# -*- mode: ruby -*-
# vi: set ft=ruby :
VAGRANTFILE_API_VERSION = "2"

require 'fileutils'

def yumi(centos)
  # inline shell commands are performed as root, while file tranfers are preformed as vagrant
  centos.vm.provision :shell, :inline => "yum update -y"
  # in case some older version of samba is present in the VM:
  centos.vm.provision :shell, :inline => "yum remove samba samba-common samba-winbind-clients samba-client"
  # centos.vm.provision :shell, :inline => "yum install -y samba4 samba4-common samba4-libs samba4-test samba4-winbind-clients samba4-client"
  centos.vm.provision :shell, :inline => "yum install -y samba4 samba4-libs samba4-test samba4-winbind-clients samba4-client"
  # cif-utils supports samba3 but not 4? conflits are reported when installing samba4-common with this version 3 ... so ...
  # centos.vm.provision :shell, :inline => "yum install -y ctdb attr selinux-policy-targeted cifs-utils policycoreutils-python"
  centos.vm.provision :shell, :inline => "yum install -y ctdb attr selinux-policy-targeted policycoreutils-python"
  centos.vm.provision :shell, :inline => "yum install -y zip unzip poppler-utils"
end

def samba(centos)
  # inline shell commands are performed as root, while file tranfers are preformed as vagrant
  # upload samba4 config files
  centos.vm.provision "file", source: "../etc/samba4/read-only_samba.conf", destination: "/home/vagrant/uploads/read-only_samba.conf"
  centos.vm.provision "file", source: "../etc/samba4/clustered_samba.conf", destination: "/home/vagrant/uploads/clustered_samba.conf"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/clustered_samba.conf /etc/samba/smb.conf"
  centos.vm.provision "file", source: "../etc/samba4/lmhosts67", destination: "/home/vagrant/uploads/lmhosts67"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/lmhosts67 /etc/samba/lmhosts"
  centos.vm.provision "file", source: "../etc/samba4/smbusers", destination: "/home/vagrant/uploads/smbusers"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/smbusers /etc/samba/smbusers"
  centos.vm.provision :shell, :inline => "smbpasswd -n -a vagrant"
  # upload ctdb files
  centos.vm.provision "file", source: "../etc/sysconfig/ctdb", destination: "/home/vagrant/uploads/ctdb"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/ctdb /etc/sysconfig/ctdb"
  centos.vm.provision "file", source: "../etc/ctdb/nodes", destination: "/home/vagrant/uploads/ctdbnodes"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/ctdbnodes /etc/ctdb/nodes"
  centos.vm.provision "file", source: "../etc/ctdb/public_addresses", destination: "/home/vagrant/uploads/ctdbpublic_addresses"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/ctdbpublic_addresses /etc/ctdb/public_addresses"
  # validate script(s)
  centos.vm.provision "file", source: "../etc/clusterstat.sh", destination: "/home/vagrant/uploads/clusterstat.sh"
end

def upload(centos)
  # inline shell commands are performed as root, while file tranfers are preformed as vagrant
  # so need to chown from root to vagrant:
  centos.vm.provision :shell, :inline => "mkdir -p /home/vagrant/uploads ; chown vagrant:vagrant /home/vagrant/uploads"
  # upload some useful files
  centos.vm.provision "file", source: "~/.balias", destination: "/home/vagrant/uploads/.balias"
  centos.vm.provision :shell, :inline => "cp -p /home/vagrant/uploads/.balias /home/vagrant/.balias"
  centos.vm.provision "file", source: "~/.gitconfig", destination: "/home/vagrant/uploads/.gitconfig"
  centos.vm.provision :shell, :inline => "cp -p /home/vagrant/uploads/.gitconfig /home/vagrant/.gitconfig"
  centos.vm.provision :shell, :inline => "echo '192.168.33.10 cos67.10' >> /etc/hosts"
  centos.vm.provision :shell, :inline => "echo '192.168.33.20 cos67.20' >> /etc/hosts"
  centos.vm.provision :shell, :inline => "echo '192.168.33.30 cos67.30' >> /etc/hosts"
  centos.vm.provision :shell, :inline => "echo '192.168.33.30 cos67.81' >> /etc/hosts"
  centos.vm.provision "file", source: "../etc/rc.local", destination: "/home/vagrant/uploads/rc.local"
  centos.vm.provision :shell, :inline => "cp /home/vagrant/uploads/rc.local /etc/rc.d/rc.local ; chmod +x /etc/rc.d/rc.local"
  samba(centos)
end

def hypervisor(centos)
  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  centos.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    # vb.gui = true
    vb.memory = "1024"
  end
end

def provision(centos)
  # note relative path indicates host directory is under host project directory, which by default
  # is mapped/sync-ed/shared as /vagrant -- but defaults may change so ...
  FileUtils.mkdir_p(['mnt/ctdb', 'mnt/share']) # make sure shared/sync'd mount directory exists on host
  centos.vm.synced_folder "mnt/ctdb", "/mnt/ctdb"
  centos.vm.synced_folder "mnt/share", "/mnt/share"
  hypervisor(centos)
  yumi(centos)
  upload(centos)
  # centos.vm.provision :shell, :inline => "service ctdb restart"
  # centos.vm.provision :shell, :inline => "service smb restart"
  # centos.vm.provision :shell, :inline => "service nmb restart"
  # or
  centos.vm.provision :shell, :inline => "sh /etc/rc.d/rc.local"
end
  
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.define "cos67.10" do |centos|
    centos.vm.box = "bento/centos-6.7" 
    centos.vm.hostname = "cos67.10"
    centos.vm.network "private_network", ip: "192.168.33.10"
    centos.vm.network :forwarded_port, guest: 22, host: 2210, id: "ssh", auto_correct: true
    provision(centos)
  end
  config.vm.define "cos67.20" do |centos|
    centos.vm.box = "bento/centos-6.7" 
    centos.vm.hostname = "cos67.20"
    centos.vm.network "private_network", ip: "192.168.33.20"
    centos.vm.network :forwarded_port, guest: 22, host: 2220, id: "ssh", auto_correct: true
    provision(centos)
  end
  config.vm.define "cos67.30" do |centos|
    centos.vm.box = "bento/centos-6.7" 
    centos.vm.hostname = "cos67.30"
    centos.vm.network "private_network", ip: "192.168.33.30"
    centos.vm.network :forwarded_port, guest: 22, host: 2230, id: "ssh", auto_correct: true
    provision(centos)
  end
end

